REPOSITORY := registry.gitlab.com/kehet/fennotes

.PHONY: all

all:
	docker build -t $(REPOSITORY) .
	docker push $(REPOSITORY)
