FROM alpine:3.7

ENV PORT 8080
EXPOSE 8080

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

COPY . /project
WORKDIR /project

RUN apk add --no-cache nodejs nginx git
RUN cd /project && npm install && npm run build && mv dist /var/www/html

ENTRYPOINT ["tail -f /var/log/nginx/*.log"]
